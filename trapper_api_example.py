## Copyright 2015 Jakub Bubnicki
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

import getpass
from StringIO import StringIO

# pip install matplotlib
import matplotlib.pyplot as plt
# pip install requests
import requests
from requests.compat import urljoin as urlj
# pip install pandas
import pandas

###########################
# ---- CONFIGURATION ---- #
###########################

# The following settings should be changed:
# the address of your TRAPPER server e.g.
HOST = 'https://demo.trapper-project.org'
# your login e.g.
LOGIN = 'trapper-project@gmail.com'

# these urls can change in the future as the software is 
# under active development
URLS = {
    'LOGIN': 'account/login/',
    'DEPLOYMENTS': 'geomap/api/deployments/export/',
    'RESULTS': 'media_classification/api/classifications/results/{cp}/'
}

#######################
# ---- FUNCTIONS ---- #
#######################

class Connection(object):
    """
    """

    def __init__(self, host, login):
        self.session = requests.Session()
        self.host = host
        self.login_url = urlj(self.host, URLS['LOGIN'])
        self.login = login
        self.password = getpass.getpass()
        # first get csrf token
        csrf_token = self.session.get(self.login_url).cookies['csrftoken']
        # prepare login data
        login_data = {
            'login': self.login,
            'password': self.password,
            'csrfmiddlewaretoken': csrf_token
        }
        headers = {'Referer': self.host}
        # POST login data
        r = self.session.post(self.login_url, data=login_data, headers=headers)
        if r.status_code == 200:
            msg = 'You have sucessfuly login to {url}.'.format(
                url=self.host
            )
        else:
            msg = 'Login failed. Status code: {code}'. format(
                code=r.status_code
            )
            self.r = r
        print msg

    def get_deployments(self, query_str=None, save_csv=True):
        api_url = urlj(self.host, URLS['DEPLOYMENTS'])
        if query_str:
            api_url = urlj(api_url, query_str)
        print 'Downloading data...'
        r = self.session.get(api_url)
        df = pandas.DataFrame.from_csv(
            StringIO(r.content)
        )
        if save_csv:
            print 'Saving data to a file (csv)...'
            df.to_csv('deployments.csv')
        return df
    
    def get_results(self, cproject, query_str=None, save_csv=True):
        api_url = urlj(
            self.host, URLS['RESULTS'].format(
                cp=str(cproject)
            )
        )
        if query_str:
            api_url = urlj(api_url, query_str)
        print 'Downloading data...'
        r = self.session.get(api_url)
        df = pandas.DataFrame.from_csv(
            StringIO(r.content)
        )
        if save_csv:
            print 'Saving data to a file (csv)...'
            df.to_csv('results.csv')
        return df


def build_occupancy_history(
        deployments, results, count_variable, offset=1, 
        return_counts=False, save_csv=True, min_visits=5,
        max_visits=None
):
    """
    This function assumes that all required filtering 
    (e.g. by species of interest) has been already done. 
    The `offset` should be expressed in days.
    It returns a data frame in a wide format.
    """
    # keys are deploymens; values are lists of detection/non-detection
    # or counts when return_counts=True
    occu_history_data = {}

    for i in range(0, len(deployments)):
        d = deployments.iloc[i]
        d_start = d.deployment_start.date()
        d_end = d.deployment_end.date()

        # number of visits (days camera was recording)
        try:
            visits = (d_end - d_start).days+1
        except TypeError:
            continue
        if visits <= min_visits:
            continue
        occu_history_data[d.deployment_id] = [0]*visits

        # get results/observations for current deployment
        d_data = results[results.deployment_id==d.deployment_id]

        # group observations by date and count_variable
        d_data = d_data.groupby(
            d_data.date_recorded.dt.date
        )[count_variable].sum().reset_index()

        for j in range(0, len(d_data)):
            observation = d_data.iloc[j]
            day = (observation.date_recorded - d_start).days
            val = 1
            if return_counts:
                val = observation[count_variable]
            try:
                occu_history_data[d.deployment_id][day] = val
            except IndexError:
                print 'Most probably wrong timestamp:'
                print 'Deployment ID: {id}'.format(id=d.deployment_id)
                print 'Deployment start: {s}'.format(s=d_start)
                print 'Deployment end: {e}'.format(e=d_end)
                print 'Observation recorded at: {o}'.format(o=observation)

        # apply the offset
        occu_history_data[d.deployment_id] = occu_history_data[d.deployment_id][offset:-offset]
    df = pandas.DataFrame.from_dict(occu_history_data, orient='index', dtype=int)
    if max_visits:
        df = df.ix[:,:max_visits]
    df.sort_index(inplace=True)
    if save_csv:
        df.to_csv('occupancy_matrix.csv')
    return df


# make custom legend for bubble plots
def make_bubble_legend(
        values, scale=200, min_size=10, title='', 
        location=4, fontsize=12, **kwargs
):
    """
    """
    tmp_plots = []
    labels = []
    for v in values: 
        t = plt.scatter(
            [],[], s=v*scale+min_size, **kwargs
        )
        tmp_plots.append(t)
        labels.append(str(v))
    leg = plt.legend(
        tmp_plots, labels, title=title, scatterpoints=1, 
        loc=location, fontsize=fontsize 
    )
    return leg


#############################
# ---- RUN THE EXAMPLE ---- #
#############################
        
print 'Making connection with TRAPPER server: {host}...'.format(
    host=HOST
)
con = Connection(HOST, LOGIN) 

# Download data from TRAPPER server 
query_str = '?species=Red+Deer&age=&behaviour=&gender='
# "2" is the classification project id in Trapper
rdf = con.get_results(2, query_str=query_str, save_csv=True)
query_str = '?research_project=3&correct_setup=True&correct_tstamp=True'
ddf = con.get_deployments(query_str=query_str, save_csv=True)

# or read from csv
rdf = pandas.read_csv('results.csv')
ddf = pandas.read_csv('deployments.csv')

rdf.date_recorded = pandas.to_datetime(rdf.date_recorded)

# filter deployments based on quality indices
ddf = ddf[ddf.view_quality != 'Exclude']
correct_deployments = ddf.deployment_id

# filter high quality data 
rdf_sel = rdf[rdf.deployment_id.isin(correct_deployments)]

# calculate how many days camera trap was recording
ddf['deployment_start'] = pandas.to_datetime(ddf['deployment_start'])
ddf['deployment_end'] = pandas.to_datetime(ddf['deployment_end'])
ddf['days'] = (ddf.deployment_end - ddf.deployment_start).dt.days

grouping_factors = ['deployment_id']
count_variable = 'number_new'

# raw counts
rdf_counts = rdf_sel.groupby(grouping_factors)[count_variable].sum()
rdf_counts = pandas.DataFrame({'counts' : rdf_counts}).reset_index()

ddf_counts = ddf.merge(
    rdf_counts, on='deployment_id', how='left'
)
# set counts NaN to zeros
ddf_counts.loc[ddf_counts['counts'].isnull(), 'counts'] = 0

print 'Calculating trapping rates...'
ddf_counts['trate'] = ddf_counts.counts/ddf_counts.days

print 'Building occupancy matrix...'
occu = build_occupancy_history(
    ddf, rdf, count_variable, offset=1, return_counts=False, 
    save_csv=True, min_visits=5, max_visits=14
)

print 'Estimating naive occupancy...'
occu_naive = pandas.DataFrame({'occu': occu.mean(axis=1, skipna=True)})
ddf_occu_naive = ddf.merge(
    occu_naive, left_on='deployment_id', right_index=True, how='right'
)

print 'Generating & saving plots...'
# Add points with trapping rates
ax = ddf_counts.plot(
    kind='scatter', x='location_X', y='location_Y', 
    s=ddf_counts['trate']*200+10, alpha=0.7, edgecolors='none', 
    zorder=3
)
# Add the first legend manually to the current Axes
leg = make_bubble_legend(
    [0,0.1,0.5,1.5], title='Trapping Rate',
    edgecolors='none', alpha=0.7
)
ax.add_artist(leg)

# Add points with naive occupancy
ddf_occu_naive.plot(
    kind='scatter', x='location_X', y='location_Y', 
    s=ddf_occu_naive['occu']*300+10, edgecolors='Red', linewidth='1', color='none',
    zorder=4, ax=ax
)
make_bubble_legend(
    [0,0.1,0.5,1], scale=300, location=3, title='Occupancy (naive)',
    color='none', edgecolors='Red', linewidth='1'  
)

plt.savefig('bubbles.pdf')


